import argparse
import logging
import os
import subprocess


class Detect:
	def __init__(self):
		super(Detect, self).__init__()

	def run_detect(self):

		project, weights, source, device, save_txt = opt.folder_path, opt.weights, opt.source, opt.device, opt.save_txt
		command = 'python ' + os.path.realpath(os.path.join('yolov7', 'detect.py')) \
				+ ' --project ' + os.path.join(project, 'runs/detect') \
				+ ' --weights ' + '"' + weights[0] + '"' \
				+ ' --source ' + source \
				+ ' --device ' + device \
				+ ' --save-txt'


		# print('測試的參數', command)

		try:
			result = subprocess.run(command, stdout=subprocess.PIPE, shell=True, check=True)

			print(result.stdout.decode('utf-8'))

		except subprocess.CalledProcessError as e:
			logging.error(f"{command} failed with error code {e.returncode}")


if __name__ == '__main__':
	# -f D:\image -w D:\image\runs\train\exp\weights\best.py -s D:\image\20220518_1437576719.png
	parser = argparse.ArgumentParser()
	# print(sys.argv[0], sys.argv[1])
	parser.add_argument('--folder-path', '-f', type=str, default='D:/', required=True, help='input local folder')
	parser.add_argument('--weights', '-w', nargs='+', type=str, default='yolov7/yolov7.pt', help='model.pt path(s)')
	parser.add_argument('--source', '-s',  type=str, default='yolov7/inference/images', help='use image test best model')  # file/folder, 0 for webcam
	parser.add_argument('--device', '-d', default='cpu', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
	parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')

	opt = parser.parse_args()
	print(opt)
	Detect().run_detect()
