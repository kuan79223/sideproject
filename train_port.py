import argparse
import glob
import logging
import subprocess
import os
import random
import shutil
from pathlib import Path


class ProcessIO:
	DATA_YAML = ''
	PERCENT = 90

	def __init__(self):
		super(ProcessIO, self).__init__()
		self.cls_dict = {}  # 存放每個 txt 的檔案名稱,代碼及數量
		self.cls_list = []  # 存放 classes.txt 的類別名稱
		self.train_img_folder = None
		self.train_labels_folder = None
		self.val_img_folder = None
		self.val_labels_folder = None
		self.ext = ''
		self.user_folder = opt.folder_path

	def io_convert(self):
		# 創建訓練資料夾
		self.train_img_folder, self.val_img_folder = create_images_folder(self.user_folder)
		self.train_labels_folder, self.val_labels_folder = create_val_folder(self.user_folder)

		# 將配對矩陣內的檔名換成 txt --------------------------------------
		for file in self.match_file(self.user_folder):
			if file != 'classes':
				with open(os.path.join(self.user_folder, file + '.txt'), 'r', encoding='utf-8') as f:
					# 將每行字首放進集合中
					prefix_set = set()
					for line in f.readlines():
						line = line.strip('')
						prefix_set.add(line[0])
					# 排序 set 讓字首的類別代碼有順序性
					prefix_list = sorted(prefix_set)
					# 宣告類別名稱
					cls = ''
					# 拼接字首
					for word in prefix_list:
						cls += word

					# 字典的KEY中有存在類別名稱
					if cls in self.cls_dict.keys():
						self.cls_dict[cls]['num'] += 1
						self.cls_dict[cls]['img_list'].append(file)
					# 否則創建字典
					else:
						self.cls_dict[cls] = {'num': 1, 'img_list': [file]}

		self.allocate_file(self.cls_dict)
		self.open_cls_text()

		# end-----------------------------------------------------------

	def allocate_file(self, __dict__):
		content = ''
		'''取得每個類別的影像數量與影像陣列內容'''
		for key, value in __dict__.items():

			# 影像陣列的數量
			cls_number = __dict__[key]['num']
			# 影像陣列的內容
			img_list = __dict__[key]['img_list']
			# 陣列的數組
			img_total = range(cls_number)
			# 比重   =  數量 * %
			train_percent = int(cls_number * self.PERCENT / 100)

			# 亂數選取多個檔案
			# 訓練數組 = 數量範圍內 , 取得多少張
			train_num = random.sample(img_total, train_percent)

			content += key + '  :  ' + str(train_percent) + '\n'

			for index in img_total:
				name = img_list[index]

				filename = name.split("\\")[-1]

				# 判斷陣列的數組是否有在訓練數組中
				if index in train_num:
					# 將索引的數組名稱加上副檔名
					shutil.copyfile(name + '.' + self.ext, os.path.join(self.train_img_folder, filename + '.' + self.ext))
					shutil.copyfile(name + '.txt', os.path.join(self.train_labels_folder, filename + '.txt'))
				else:
					# 剩下的給 val
					shutil.copyfile(name + '.' + self.ext, os.path.join(self.val_img_folder, filename + '.' + self.ext))
					shutil.copyfile(name + '.txt', os.path.join(self.val_labels_folder, filename + '.txt'))

	# 新增 yaml
	def add_yaml(self, folder):
		# 在專案資料夾內創建 data 資料夾
		train_data = os.path.join(folder, 'data')
		Path(train_data).mkdir(parents=True, exist_ok=True)
		# 創建 yaml 檔案
		return os.path.join(train_data, 'data.yaml')

	# 打開 labelimg 生成的 classes.txt
	def open_cls_text(self):

		with open(check_cls_text(self.user_folder), 'r') as file:
			for line in file.readlines():
				# 將類別名稱放進暫存中
				self.cls_list.append(line.strip('\n'))
		# 將路徑反斜線 取代成 單斜線
		train_img_dir = str(self.train_img_folder).replace('\\', '/')
		val_img_dir = str(self.val_img_folder).replace('\\', '/')

		# yaml 的內容
		data = 'train' + ': ' + train_img_dir + '\n' + 'val' + ': ' + val_img_dir + '\n' \
			+ 'nc' + ': ' + str(len(self.cls_list)) + '\n' + 'names' + ': ' + str(self.cls_list)

		self.DATA_YAML = self.add_yaml(self.user_folder)

		with open(self.DATA_YAML, 'w') as file:
			file.write(data)


	# 配對資料來源
	def match_file(self, folder):
		source_img = []
		source_file = []
		if folder is not None:

			for path, subdir, name in os.walk(folder):

				for img_path in glob.glob(os.path.join(path, '*.png')) + glob.glob(os.path.join(path, '*.jpg')) + glob.glob(os.path.join(path, '*.jpeg')):
					img_basename, self.ext = os.path.basename(img_path).split('.')

					source_img.append(os.path.join(path, img_basename))

				for txt_path in glob.glob(os.path.join(path, '*.txt')):
					txt_basename = os.path.basename(txt_path).split('.')[0]
					source_file.append(os.path.join(path, txt_basename))

			return set(source_img) & set(source_file)
		else:
			return None


# 確認有檔案內否有無classes
def check_cls_text(folder):
	# 在資料來源中搜尋 cls.txt
	if 'classes.txt' in os.listdir(folder):
		cls_path = os.path.join(folder, 'classes.txt')
		return cls_path
	else:
		return None


# 創建 train,val 的 images
def create_images_folder(folder):
	# 訓練集
	output = Path(folder)
	train_images = output.joinpath('train', 'images')
	val_images = output.joinpath('val', 'images')

	Path(train_images).mkdir(parents=True, exist_ok=True)
	Path(val_images).mkdir(parents=True, exist_ok=True)

	return train_images, val_images


# 創建 train,val 的 labels
def create_val_folder(folder):
	# 驗證集
	output = Path(folder)
	train_labels = output.joinpath('train', 'labels')
	val_labels = output.joinpath('val', 'labels')
	Path(train_labels).mkdir(parents=True, exist_ok=True)
	Path(val_labels).mkdir(parents=True, exist_ok=True)

	return train_labels, val_labels


class Train:
	def __init__(self):
		super(Train, self).__init__()
		self.process_io = ProcessIO()

	def run_train(self):

		project, weights, img_size, batch_size, epochs, device = opt.folder_path, opt.weights, opt.img_size, opt.batch_size, opt.epochs, opt.device

		self.process_io.io_convert()

		data_yaml = self.process_io.DATA_YAML

		# print(os.getcwd())

		command = 'python ' + os.path.realpath(os.path.join('yolov7', 'train.py')) \
				+ ' --weights ' + weights \
				+ ' --data ' + data_yaml \
				+ ' --project ' + os.path.join(project, 'runs/train') \
				+ ' --img-size ' + str(img_size[0]) \
				+ ' --batch-size ' + str(batch_size)\
				+ ' --epochs ' + str(epochs) \
				+ ' --device ' + device

		try:
			result = subprocess.run(command, stdout=subprocess.PIPE, shell=True, check=True)

			print(result.stdout.decode('utf-8'))

		except subprocess.CalledProcessError as e:
			logging.error(f"{command} failed with error code {e.returncode}")


if __name__ == '__main__':

	parser = argparse.ArgumentParser()

	parser.add_argument('--folder-path', '-f', type=str, default='D:/', required=True, help='input local folder')
	parser.add_argument('--weights', '-w', type=str, default='yolov7.pt', help='initial weights path')
	parser.add_argument('--img-size', '-imgz', nargs='+', type=str, default=[640], help='train image sizes')
	parser.add_argument('--batch-size', '-b', type=int, default=16, required=True, help='total batch size for all GPUs')
	parser.add_argument('--epochs', '-e', required=True, type=int, default=300, help='you want to train how many times ')
	parser.add_argument('--device', '-d', required=True, default='cpu', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')

	opt = parser.parse_args()
	print(opt)

	Train().run_train()